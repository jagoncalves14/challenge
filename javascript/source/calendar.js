// CHECK IF YEAR IS A LEAP YEAR
function leapYear(year){
	year = parseInt(year);
  	return ((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0);
}

// CALENDAR FUNCTION
var CALENDAR = function () { 
    var wrap, 
    	label,  
        months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

    // CALENDAR INITIALIZER
    function init(newWrap) { 
		var todayDate = new Date();
		var todayDay = todayDate.getDate();  //it returns the date 
		var todayMonth = todayDate.getMonth();
		var todayYear = todayDate.getFullYear();

		wrap = $(newWrap || "#calendar"); 
		label = wrap.find("#label"); 
		wrap.find("#prev").bind("click.calendar", function () { 
			switchMonth(false); 
		}); 
		wrap.find("#next").bind("click.calendar", function () { 
			switchMonth(true);  
		}); 
		label.bind("click", function () { 
			switchMonth(null, new Date().getMonth(), new Date().getFullYear()); });        
		label.click();
    }

    // SWITCH MONTH EVENT
    function switchMonth(next, month, year) { 

		var curr = label.text().trim().split(" "), calendar, tempYear =  parseInt(curr[1], 10); 
		month = month || ((next) ? ( (curr[0] === "December") ? 0 : months.indexOf(curr[0]) + 1 ) : ( (curr[0] === "January") ? 11 : months.indexOf(curr[0]) - 1 )); 
		year = year || ((next && month === 0) ? tempYear + 1 : (!next && month === 11) ? tempYear - 1 : tempYear);

		if (!month) {
		    if (next) {
		        if (curr[0] === "December") {
		            month = 0;
		        } else {
		            month = months.indexOf(curr[0]) + 1;
		        }
		    } else {
		        if (curr[0] === "January") {
		            month = 0;
		        } else {
		            month = months.indexOf(curr[0]) - 1;
		        }
		    }
		}

		if (!year) {
		    if (next && month === 0) {
		        year = tempYear + 1;
		    } else if (!next && month === 11) {
		        year = tempYear - 1;
		    } else {
		        year = tempYear;
		    }
		}

		//FIX JANUARY BUG ON INIT
	 	if(next == null){
	 		if(month == 11){
	 			month = 0;
	 		}
	 	}

		calendar =  createCal(year, month);

		$("#calendar-footer", wrap)
	            .find(".current")
                .removeClass("current")
                .addClass("temp")
	            .end()
	            .prepend(calendar.calendar())
	            .find(".temp")
                .fadeOut(function () {
                	$(this).remove();
                });
 
        $('#label').text(calendar.label);

        console.log('Month being shown: '+months[month]);
    }

	// CREATE CALENDARY
    function createCal(year, month) {
    	var february = 28;
    	if(leapYear(year) == true){
    		february = 29;
    	} else{
    		february = 28;
    	}
    	// console.log(year);
    	// console.log(february);
    	var day = 1,
    		i, j, haveDays = true,
	        startDay = new Date(year, month, day).getDay(),
	        daysInMonths = [
	        	31,
	        	february,
	        	31,
	        	30,
	        	31,
	        	30,
	        	31,
	        	31,
	        	30,
	        	31,
	        	30,
	        	31
        	],
	        calendar = [];

        if (createCal.cache[year]) {
		    if (createCal.cache[year][month]) {
		        return createCal.cache[year][month];
		    }
		} else {
		    createCal.cache[year] = {};
		}

		i = 0;
		while (haveDays) {
		    calendar[i] = [];

		    for (j = 0; j < 7; j++) {
		        if (i === 0) {
		            if (j === startDay) {
		                calendar[i][j] = day++;
		                startDay++;
		            }
		        } else if (day <= daysInMonths[month]) {
		            calendar[i][j] = day++;
		        } else {
		            calendar[i][j] = "";
		            haveDays = false;
		        }
		        if (day > daysInMonths[month]) {
		            haveDays = false;
		        }
		    }
		    i++; 
	   	}

		for (i = 0; i < calendar.length; i++) { 
		    calendar[i] = "<tr><td>" + calendar[i].join("</td><td>") + "</td></tr>"; 
		}

		calendar = $("<table>" + calendar.join("") + "</table>").addClass("current");
	 
	 	//ADD NULL C
		$("td:empty", calendar).addClass("null");
		

		//FIND TODAY DATE
		if (month === new Date().getMonth()) {
		    $('td', calendar).filter(function () {
		    	return $(this).text() === new Date().getDate().toString(); }).addClass("today");
		}
		createCal.cache[year][month] = { calendar : function () { return calendar.clone() }, label : months[month] + " " + year };
	 
		return createCal.cache[year][month];
    }

    createCal.cache = {};

    return { 
        init : init,
        switchMonth : switchMonth,
        createCal   : createCal
    };
}

function getDate(el){
	// Transform date in Unix Timestamp

	Date.prototype.getUnixTime = function() { 
		return this.getTime()/1000|0 
	};
	var getDay = el.text(),
		getLabel = $('#label').text().split(" "),
		getMonth = getLabel[0],
		getYear = getLabel[1],
		dateString = getDay+' '+getMonth+' '+getYear,
		unitTimeStamp = new Date(getDay+' '+getMonth+' '+getYear).getUnixTime();

	console.log('Last selected date: '+dateString);

	$('table.current td').removeClass('active');
	el.addClass('active');
	$('#date_string').html(dateString);
	$('#date_timestamp').html(unitTimeStamp);
}

$(document).ready(function() {
	var cal = CALENDAR();
	cal.init();
	
	$(document).on('click','table.current td', function(){
		var $this = $(this);
    	getDate($this);
    });
});




