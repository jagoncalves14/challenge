// @codekit-prepend "../vendor/jquery/dist/jquery.min.js"
// @codekit-prepend "calendar.js"

/* Toggle state of the main menu | Mobile only */
$('.js-menu').click(function() {
    $('svg.menu').toggleClass('open');
    $('header').toggleClass('open');
});

$('[data-element="right-panel-arrow"]').click(function() {
    $('[data-element="right-panel"]').toggleClass('open');
});